// HelloWorld.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <cmath>
#include <string>


//������ �����

class Animals
{


public:
    //�����������
    Animals()
    {

    }

    virtual void voice()
    {
        std::cout << "default sound" << '\n';
    }

     virtual ~Animals()
    {
      
    }


};

class Dog : public Animals
{

public:
    void voice() override
    {
        std::cout << "Woof!" << '\n';
    }

    ~Dog() override
    {

    }

};

class Cat : public Animals
{
 
    void voice() override
    {
        std::cout << "Meu!" << '\n';
    }

    ~Cat() override
    {

    }
};

class Cow : public Animals
{
    void voice() override
    {
        std::cout << "Mooooo!" << '\n';
    }

    ~Cow() override
    {

    }

};

class Pig : public Animals
{
    void voice() override
    {
        std::cout << "Uweeeee!" << '\n';
    }

    ~Pig() override
    {

    }

};


int main()
{

   Animals* farm[4];

   farm[0] = new Dog();
   farm[1] = new Cat();
   farm[2] = new Cow();
   farm[3] = new Pig();

   for (int i = 0; i < 4; i++)
   {
       farm[i]->voice();
       delete farm[i]; //����������� ������
   }
  
   std::system("pause");
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
