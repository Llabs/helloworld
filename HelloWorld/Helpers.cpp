#include <cmath>

int sum(int a, int b)
{
    return pow(a, 2) + 2 * a * b + pow(b, 2);
}
